angular.module('ims').service('DashboardService', ['$http',  function ($http) {
  //var API_URL = 'routes/api/v1/';

  var DashboardService = {
        saveprayerrequest: function (userid,username,useremailid,userphonenumber,useraddress,userprayerrequest) {
            return $http.post('http://qikdots.com/imsapi/public/api/v1/prayer' , {userid:userid,username:username,useremailid:useremailid,userphonenumber:userphonenumber,useraddress:useraddress,userprayerrequest:userprayerrequest});
        },
        saveregisterusers: function (name,username,userpassword,userlocation,useremailid,userphonenumber) {
            return $http.post('http://qikdots.com/imsapi/public/api/v1/signup' , {name:name,username:username,userpassword:userpassword,userlocation:userlocation,useremailid:useremailid,userphonenumber:userphonenumber});
        },
      }
      return DashboardService;
}]);
