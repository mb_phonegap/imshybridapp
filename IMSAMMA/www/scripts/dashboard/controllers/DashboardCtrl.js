/*
 * IMS Dashboard Controller Script
 * http://minusbugs.com/
 * Created By: Abhijith A Nair
 * Modified By: Abhijith A Nair
 * Created On: 03/03/2017
 * Modified On:03/03/2017
 */

(function() {
    angular.module('ims')
        //.controller('DashboardCtrl', DashboardCtrl);


    .controller('DashboardCtrl', ['$scope','$window','DashboardService','$state', function($scope,$window,DashboardService,$state) {

        $scope.saveprayer = function(inputName, inputEmailAddress, inputPhoneNumber, inputContactAddress, inputPrayerRequest) {
            // alert("hai");
            $scope.UserId =  $window.localStorage['key'];
            $scope.Name = inputName;
            $scope.Email = inputEmailAddress;
            $scope.PhoneNumber = inputPhoneNumber;
            $scope.ContactAddress = inputContactAddress;
            $scope.PrayerRequest = inputPrayerRequest;
            DashboardService.saveprayerrequest($scope.UserId,$scope.Name,$scope.Email,$scope.PhoneNumber,$scope.ContactAddress,$scope.PrayerRequest).success(function(response) {
                        $scope.requestsave = JSON.parse(response.resultCode);

                        if ($scope.requestsave  == 1) {
                             var alertPopup = $ionicPopup.alert({
         title: 'Title',
         template: 'Alert message'
      });
                        	//alert("added successfully")
                        } else {
                            alert("Failed")
                        }

                    })
        }

        $scope.registerusers = function(inputName,inputUserName,inputPassword,inputLocation,inputEmailId,inputPhoneNumber) {
            // alert("hai");
            //$scope.Name = $('#name').val();
            $scope.Name = inputName;
            $scope.UserName = inputUserName;
            $scope.Password = inputPassword;
            $scope.Location = inputLocation;
            $scope.EmailId = inputEmailId;
            $scope.PhoneNumber = inputPhoneNumber;
            DashboardService.saveregisterusers($scope.Name, $scope.UserName,$scope.Password,$scope.Location,$scope.EmailId,$scope.PhoneNumber).success(function(response) {
                        $scope.requestsave = JSON.parse(response.resultCode);
                        // $localStorage.userid = response.userid;
                        if ($scope.requestsave  == 1) {
                            alert("User has been updated");
                            $window.localStorage['key'] = response.userid;
                        }
                        else if ($scope.requestsave  == 2) {
                             $state.go('app.dashboard');
                            $window.localStorage['key'] = response.userid;
                        } else {
                            alert("Failed");
                        }

                    })
        }
    }]);




    //    dashCtrl.$inject = ['$scope'];

    //    $scope.homeAction = function() {
    //  	alert("hai");
    // };

    function dashCtrl(_scope) {
        alert("hai");
    }
})();
